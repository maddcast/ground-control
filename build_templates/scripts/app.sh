#!/bin/bash
# nohup-based java app runner.
# Parameters: <ACTION> = start|stop. start action also stops app before run;
 

# Detecting script directory {
SCRIPT_PATH="${BASH_SOURCE[0]}";
if ([ -h "${SCRIPT_PATH}" ]) then
  while([ -h "${SCRIPT_PATH}" ]) do SCRIPT_PATH=`readlink "${SCRIPT_PATH}"`; done
fi
pushd . > /dev/null
cd `dirname ${SCRIPT_PATH}` > /dev/null
SCRIPT_PATH=`pwd`;
SCRIPT_PATH=`readlink -f ${SCRIPT_PATH}`
CURRENT_PATH="${SCRIPT_PATH}"
cd $CURRENT_PATH > /dev/null
# }


# java gc presets to use in settings
JVM_OPTS_COMMON=" -server -Djava.awt.headless=true -Dlog4j.configurationFile=$(pwd)/logger.xml \
    -Djava.net.preferIPv4Stack=true -Duser.timezone=Europe/Moscow "

# -Dsun.net.inetaddr.ttl=120

JVM_OPTS_GC_CMS=" -XX:+UseCompressedOops -XX:+UseParNewGC -XX:+UseConcMarkSweepGC \
    -XX:CMSInitiatingOccupancyFraction=75 -XX:+HeapDumpOnOutOfMemoryError -XX:+DisableExplicitGC \
    -XX:+UseCMSInitiatingOccupancyOnly -XX:+ScavengeBeforeFullGC -XX:+CMSScavengeBeforeRemark "

JVM_OPTS_GC_LOGGING="-XX:+PrintGCDateStamps -verbose:gc -XX:+PrintGCDetails -Xloggc:gc_log \
    XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M"



# include settings
source $CURRENT_PATH/settings.sh

if [[ -n  "$APP_USER" ]]; then
	SCRIPT_USER=`whoami`
	echo $SCRIPT_USER $APP_USER $1
	if [[ "$SCRIPT_USER" != "$APP_USER" ]]; then
		echo "Running script with APP_USER =" $APP_USER
		sudo -u $APP_USER $SCRIPT_PATH/app.sh $1
		exit 0
	fi
fi

if [[ "$1" == "stop" || "$1" == "start" ]]; then
	if [ -f "$SCRIPT_PATH/process.pid" ]; then
		RSJVM_PID=`cat $SCRIPT_PATH/process.pid`
		echo Killing java process with pid: $RSJVM_PID
		echo "sending sigterm to process $RSJVM_PID"
		kill $RSJVM_PID
		killcountdown=20
		processexists=0
		while [ $killcountdown -gt 0 ] && [ -n "$processexists" ]; do
		    sleep 1
		    let killcountdown-=1
		    processexists=$(ps -p $RSJVM_PID -o comm=)
		done
		if [[ -n "$(ps -p $RSJVM_PID -o comm=)" ]]; then
		    kill -9 $RSJVM_PID
		fi
		rm $SCRIPT_PATH/process.pid
	else 
		echo "(!) Nothing to stop. No process.pid file found"
	fi
	
fi

if [[ "$1" == "start" ]]; then
###########  Parameters to edit {
	if [[ -z "$JVM_OPTS" ]]; then
		JVM_OPTS="-Xmx1024M -XX:-OmitStackTraceInFastThrow"
	fi

	if [[ -z "$LOCALE" ]]; then
		LOCALE="-Duser.language=en -Duser.country=US"
	fi

	############ }

	echo JVM Options: $JVM_OPTS
	echo Locale: $LOCALE
	echo Main Class: $MAIN_CLASS
	echo App Params: $APP_PARAMS

	echo "Starting JVM. Working Dir: `pwd`"

	(nohup java -server $LOCALE $JVM_OPTS -jar $JAR_FILE >/dev/null 2>/dev/null </dev/null)&
	echo $! > "$SCRIPT_PATH/process.pid"
fi
