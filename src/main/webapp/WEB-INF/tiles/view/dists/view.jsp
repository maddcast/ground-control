<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gc" %>

<c:if test="${viewDistModel.hasIcon}">
    <img src="/icon/${viewDistModel.project.id}.png">
</c:if>

<h1>${viewDistModel.project.name}</h1>

<gc:widgetbox title="View distribution">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Value</th>
            <th>Current Path</th>
            <th>Exists</th>
            <th>Can read</th>
            <th>Can write</th>
            <th>Can run</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="fileParam" items="${viewDistModel.fileParams}">
            <tr>
                <td>${fileParam.title}</td>
                <td>${fileParam.rawValue}</td>
                <td>${fileParam.currentPath}</td>
                <td><gc:boolean value="${fileParam.exists}"/></td>
                <td><gc:boolean value="${fileParam.canRead}"/></td>
                <td><gc:boolean value="${fileParam.canWrite}"/></td>
                <td><gc:boolean value="${fileParam.canExecute}"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    Build Link Path: ${viewDistModel.versionDirName}<br><br>
    <a class="btn btn-primary" href="<c:url value="/dists/edit/?projectId=${viewDistModel.projectId}"/>">Edit</a>
</gc:widgetbox>

<gc:widgetbox title="Versions">
    <c:forEach var="version" items="${viewDistModel.versions}">
        <c:choose>
            <c:when test="${version.current}">
                <b>${version.number}</b><br>
            </c:when>
            <c:otherwise>
                ${version.number}<br>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <br>
    <a class="btn btn-primary" href="<c:url value="/versions/pre_upload/?projectId=${viewDistModel.projectId}"/>">Upload new</a>
    <a class="btn btn-primary" href="<c:url value="/versions/list/?projectId=${viewDistModel.projectId}"/>">Switch version</a>
</gc:widgetbox>

<gc:widgetbox title="Deploy Script">
    <a class="btn btn-primary" href="<c:url value="/dists/script/edit/?projectId=${viewDistModel.projectId}"/>">Edit script</a>
</gc:widgetbox>

<gc:widgetbox title="Control">
    <c:if test="${viewDistModel.starter.canStart()}">
        <a class="btn btn-primary" href="<c:url value="/process/start/${viewDistModel.projectId}"/>">Start process</a>
    </c:if>
    <c:if test="${viewDistModel.starter.canStop()}">
        <a class="btn btn-primary" href="<c:url value="/process/stop/${viewDistModel.projectId}"/>">Stop process</a>
    </c:if>
    <c:if test="${viewDistModel.starter.canRestart()}">
        <a class="btn btn-primary" href="<c:url value="/process/restart/${viewDistModel.projectId}"/>">Restart process</a>
    </c:if>
    <a class="btn" href="<c:url value="/versions/pre_upload/?projectId=${viewDistModel.projectId}"/>">Check process</a>
    <c:if test="${viewDistModel.starter.canReadLog()}">
        <a class="btn btn-primary" href="<c:url value="/log/?projectId=${viewDistModel.projectId}"/>">Log</a>
    </c:if>
</gc:widgetbox>