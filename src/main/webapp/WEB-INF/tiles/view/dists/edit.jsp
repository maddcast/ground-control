<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gc" %>

<gc:widgetbox title="Editing distribution">
    <form:form cssClass="form-horizontal" servletRelativeAction="/dists/save" method="post" enctype="multipart/form-data" commandName="editDistModel">
        <form:hidden path="id"/>
        <form:hidden path="projectId"/>
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="basePath">Base Path</label>
                <div class="controls">
                    <form:input path="basePath" cssClass="input-xxlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="deployPath">Deploy Path</label>
                <div class="controls">
                    <form:input path="deployPath" cssClass="input-xxlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="versionDirName">Version Dir Name</label>
                <div class="controls">
                    <form:input path="versionDirName" cssClass="input-xxlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="logPath">Log Path</label>
                <div class="controls">
                    <form:input path="logPath" cssClass="input-xxlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="startPath">Start Script Path</label>
                <div class="controls">
                    <form:input path="startPath" cssClass="input-xxlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="stopPath">Stop Script Path</label>
                <div class="controls">
                    <form:input path="stopPath" cssClass="input-xxlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="restartPath">Restart Script Path</label>
                <div class="controls">
                    <form:input path="restartPath" cssClass="input-xxlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="pidPath">Pid Path</label>
                <div class="controls">
                    <form:input path="pidPath" cssClass="input-xxlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="pidPath">Icon</label>
                <div class="controls">
                    <form:input path="iconFile" type="file"/>
                </div>
            </div>
            <div class="form-actions">
                <form:button class="btn btn-primary">Save</form:button>
                <a class="btn" href="<c:url value="/projects/list/?parentId=${editDistModel.projectId}"/>">Cancel</a>
            </div>
        </fieldset>
    </form:form>
</gc:widgetbox>

<gc:widgetbox title="Variables">
    <code>%user_home%</code> - Home dir of current user<br>
    <code>%base_path%</code> - Base dir for distribution<br>
    <code>%version%</code> - Version number<br>
</gc:widgetbox>