<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gc" %>

<gc:widgetbox title="Editing script">
    <form:form cssClass="form-horizontal" servletRelativeAction="/dists/script/save" method="post" enctype="multipart/form-data" commandName="editDeployScriptModel">
        <form:hidden path="projectId"/>
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="script">Script</label>
                <div class="controls">
                    <form:textarea path="script" style="width: 98%;" rows="25"/>
                </div>
            </div>
            <div class="form-actions">
                <form:button class="btn btn-primary">Save</form:button>
                <a class="btn" href="<c:url value="/dists/view/?projectId=${editDeployScriptModel.projectId}"/>">Cancel</a>
            </div>
        </fieldset>
    </form:form>
</gc:widgetbox>
