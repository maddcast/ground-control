<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gc" %>

<gc:widgetbox title="Log">
    <form:form cssClass="form-horizontal" servletRelativeAction="/log" method="post" commandName="logModel">
        <form:hidden path="projectId"/>
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="log">Log</label>
                <div class="controls">
                    <form:textarea path="log" style="width: 98%;" rows="25"/>
                </div>
            </div>
            <div class="form-actions">
                <form:button class="btn btn-primary">Refresh</form:button>
                <a class="btn" href="<c:url value="/dists/view/?projectId=${logModel.projectId}"/>">Back</a>
            </div>
        </fieldset>
    </form:form>
</gc:widgetbox>
