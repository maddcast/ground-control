<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gc" %>

<c:if test="${fn:length(versionUploadModel.versions) > 0}">
    <gc:widgetbox title="Versions">
        <c:forEach var="version" items="${versionUploadModel.versions}">
            <c:choose>
                <c:when test="${version.current}">
                    <b>${version.number}</b><br>
                </c:when>
                <c:otherwise>
                    ${version.number}<br>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <br>
        <a class="btn btn-primary" href="<c:url value="/versions/list/?projectId=${versionUploadModel.projectId}"/>">Switch version</a>
    </gc:widgetbox>
</c:if>

<gc:widgetbox title="Upload zip version">
    <form:form cssClass="form-horizontal" servletRelativeAction="/versions/upload/zip" method="post" enctype="multipart/form-data" commandName="versionUploadModel">
        <form:hidden path="projectId"/>
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="versionFile">Version File</label>
                <div class="controls">
                    <form:input type="file" path="versionFile" cssClass="input-xxlarge"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="versionNumber">Version number (optional)</label>
                <div class="controls">
                    <form:input path="versionNumber" cssClass="input-xxlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="extractArchive">Extract archive</label>
                <div class="controls">
                    <form:checkbox path="extractArchive" cssClass="input-xxlarge"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="deflateFiles">Gzip files</label>
                <div class="controls">
                    <form:checkbox path="deflateFiles" cssClass="input-xxlarge"/>
                </div>
            </div>
            <div class="form-actions">
                <form:button class="btn btn-primary">Upload</form:button>
                <a class="btn" href="<c:url value="/dists/view/?projectId=${versionUploadModel.projectId}"/>">Cancel</a>
            </div>
        </fieldset>
    </form:form>
</gc:widgetbox>

<gc:widgetbox title="Upload Teamcity Artifact">
    <form:form cssClass="form-horizontal" servletRelativeAction="/versions/upload/teamcity_artifact" method="post" enctype="multipart/form-data" commandName="versionUploadModel">
        <form:hidden path="projectId"/>
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="teamcityArtifactUrl">Teamcity Artifact Url</label>
                <div class="controls">
                    <form:input path="teamcityArtifactUrl" cssClass="input-xxlarge"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="teamcityUsername">Teamcity Username</label>
                <div class="controls">
                    <form:input path="teamcityUsername" cssClass="input-xxlarge"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="teamcityPassword">Teamcity Password</label>
                <div class="controls">
                    <form:password path="teamcityPassword" cssClass="input-xxlarge"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="versionNumber">Version number (optional)</label>
                <div class="controls">
                    <form:input path="versionNumber" cssClass="input-xxlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="extractArchive">Extract archive</label>
                <div class="controls">
                    <form:checkbox path="extractArchive" cssClass="input-xxlarge"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="deflateFiles">Gzip files</label>
                <div class="controls">
                    <form:checkbox path="deflateFiles" cssClass="input-xxlarge"/>
                </div>
            </div>
            <div class="form-actions">
                <form:button class="btn btn-primary">Upload</form:button>
                <a class="btn" href="<c:url value="/dists/view/?projectId=${versionUploadModel.projectId}"/>">Cancel</a>
            </div>
        </fieldset>
    </form:form>
</gc:widgetbox>
