<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gc" %>

<c:if test="${fn:length(versionSwitchModel.versions) > 0}">
    <gc:widgetbox title="Versions">
        <form:form cssClass="form-horizontal" servletRelativeAction="/versions/switch" commandName="versionSwitchModel">
            <form:hidden path="projectId"/>
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="currentVersion">Versions</label>
                    <div class="controls">
                        <c:forEach var="version" items="${versionSwitchModel.versions}">
                            <label class="radio">
                                <form:radiobutton path="currentVersion" value="${version.number}"/> ${version.number}
                            </label>
                            <br>
                        </c:forEach>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="restart">Restart</label>
                    <div class="controls">
                        <form:checkbox path="restart"/>
                    </div>
                </div>

                <div class="form-actions">
                    <form:button class="btn btn-primary">Switch Version</form:button>
                    <a class="btn" href="<c:url value="/dists/view/?projectId=${versionSwitchModel.projectId}"/>">Cancel</a>
                </div>
            </fieldset>
        </form:form>
    </gc:widgetbox>
</c:if>

<gc:widgetbox title="Upload">
    <a class="btn btn-primary" href="<c:url value="/versions/pre_upload/?projectId=${versionSwitchModel.projectId}"/>">Upload new</a>
</gc:widgetbox>