<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gc" %>

<c:if test="${fn:length(projectTreeModel.tree.projects) > 0}">
    <gc:widgetbox title="Tree">
        <gc:projecttree nodes="${projectTreeModel.tree.projects}" dists="${projectTreeModel.dists}"/>
    </gc:widgetbox>
</c:if>