<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gc" %>

<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="<c:url value="/"/>">Home</a>
        <i class="icon-angle-right"></i>
    </li>
    <c:forEach var="project" items="${projectListModel.projectParents}">
        <li>
            <a href="<c:url value="/projects/list/?parentId=${project.id}"/>">${project.name}</a>
            <i class="icon-angle-right"></i>
        </li>
    </c:forEach>
    <li>
        ${projectListModel.currentProject.name}
    </li>
</ul>

<c:if test="${fn:length(projectListModel.projects) > 0}">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Project</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="project" items="${projectListModel.projects}">
            <tr>
                <td>
                    <a href="<c:url value="/projects/list/?parentId=${project.id}"/>">${project.name}</a>
                </td>
                <td>
                    <a class="btn btn-primary" href="<c:url value="/projects/edit/${project.id}"/>">Edit</a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>

<a class="btn btn-primary" href="<c:url value="/projects/edit/${projectListModel.currentProject.id}"/>">Edit Project</a> <a class="btn btn-primary" href="<c:url value="/projects/add/?parentId=${projectListModel.parentId}"/>">Add Sub Project</a>
<br><br>

<c:if test="${projectListModel.dist != null}">

    <gc:widgetbox title="Distribution">

        <a class="btn btn-primary" href="<c:url value="/dists/view/?projectId=${projectListModel.dist.projectId}"/>">View</a>
        <a class="btn btn-primary" href="<c:url value="/dists/edit/?projectId=${projectListModel.dist.projectId}"/>">Edit</a>

    </gc:widgetbox>

</c:if>

