<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gc" %>

<gc:widgetbox title="Editing Project">
    <form:form cssClass="form-horizontal" servletRelativeAction="/projects/save" method="post" commandName="editProjectModel">
        <form:hidden path="id"/>
        <form:hidden path="parentId"/>
        <fieldset>
            <div class="control-group">
                <label class="control-label" for="name">Name</label>
                <div class="controls">
                    <form:input path="name" cssClass="input-xlarge" autocomplete="false"/>
                </div>
            </div>
            <div class="form-actions">
                <form:button class="btn btn-primary">Save</form:button>
                <a class="btn" href="<c:url value="/projects/list/?parentId=${editProjectModel.parentId}"/>">Cancel</a>
            </div>
        </fieldset>
    </form:form>
</gc:widgetbox>

<c:if test="${editProjectModel.id != null}">

    <gc:widgetbox title="Distribution">

        <c:if test="${projectListModel.dist == null}">
            <a class="btn btn-primary" href="<c:url value="/dists/add/?projectId=${editProjectModel.id}"/>">Create Distribution</a>
        </c:if>

    </gc:widgetbox>

</c:if>
