<%@ tag pageEncoding="utf-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ attribute name="title" required="true" type="java.lang.String" %>

<div class="row-fluid">

    <div class="box span12">
        <div class="box-header">
            <h2><i class="halflings-icon th"></i><span class="break"></span>${title}</h2>
        </div>
        <div class="box-content">
            <jsp:doBody/>
        </div>
    </div>

</div><!--/row-->
