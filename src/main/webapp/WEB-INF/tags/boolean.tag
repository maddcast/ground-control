<%@ tag pageEncoding="utf-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ attribute name="value" required="true" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
    <c:when test="${value}">
        <img src="<c:url value="/img/yes16.png"/>" alt="yes">
    </c:when>
    <c:otherwise>
        <img src="<c:url value="/img/no16.png"/>" alt="no">
    </c:otherwise>
</c:choose>