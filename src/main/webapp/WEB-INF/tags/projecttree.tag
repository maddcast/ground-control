<%@ tag pageEncoding="utf-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ attribute name="nodes" required="true" type="java.util.List<com.deploist.groundcontrol.domain.Project>" %>
<%@ attribute name="dists" required="true" type="java.util.Map<java.lang.String, com.deploist.groundcontrol.domain.Distribution>" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="gc" %>

<ul>
    <c:forEach var="node" items="${nodes}">
        <li>
            ${node.name}
            <c:if test="${dists.containsKey(node.id)}">
                &nbsp;&nbsp;<a class="btn btn-primary" href="<c:url value="/dists/view/?projectId=${node.id}"/>">View</a>
            </c:if>
            <gc:projecttree nodes="${node.projects}" dists="${dists}"/>
        </li>
    </c:forEach>
</ul>
