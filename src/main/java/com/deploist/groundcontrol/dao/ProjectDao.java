package com.deploist.groundcontrol.dao;

import com.deploist.groundcontrol.domain.Project;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 22:25
 */
@Repository
@Scope("singleton")
public class ProjectDao
{
    private static final AtomicReference<Project> projectTree = new AtomicReference<>();

    private final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());


    @PostConstruct
    public void init() throws IOException
    {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        File configFile = getConfigFile();
        Project rootProject;

        if (configFile.exists())
        {
            rootProject = objectMapper.readValue(FileUtils.readFileToByteArray(configFile), Project.class);
        }
        else
        {
            rootProject = new Project();
            rootProject.setId(UUID.randomUUID().toString());
            rootProject.setName("Root");
        }
        projectTree.set(rootProject);
    }


    private File getConfigFile()
    {
        return new File("config/projects.yaml");
    }


    public Project getTree()
    {
        return projectTree.get();
    }


    public void saveTree() throws IOException
    {
        FileUtils.writeByteArrayToFile(getConfigFile(), objectMapper.writeValueAsBytes(projectTree.get()));
    }
}
