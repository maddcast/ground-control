package com.deploist.groundcontrol.dao;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 22:47
 */
public interface GenericDao<ObjClass, IdClass>
{
    ObjClass get(IdClass id);
}
