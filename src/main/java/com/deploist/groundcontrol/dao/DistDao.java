package com.deploist.groundcontrol.dao;

import com.deploist.groundcontrol.domain.Distribution;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 22:25
 */
@Repository
@Scope("singleton")
public class DistDao
{
    private static final Map<String, Distribution> distributions = new ConcurrentHashMap<>();

    private final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());


    @PostConstruct
    public void init() throws IOException
    {
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        File configFile = getConfigFile();

        if (configFile.exists())
        {
            distributions.putAll(objectMapper.readValue(FileUtils.readFileToByteArray(configFile), new TypeReference<HashMap<String, Distribution>>() {}));
        }
    }


    private File getConfigFile()
    {
        return new File("config/distributions.yaml");
    }


    public Map<String, Distribution> getAll()
    {
        return distributions;
    }


    public void saveAll() throws IOException
    {
        FileUtils.writeByteArrayToFile(getConfigFile(), objectMapper.writeValueAsBytes(distributions));
    }
}
