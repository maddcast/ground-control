package com.deploist.groundcontrol.interceptors;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by maddcast.
 */
public class VersionInterceptor implements HandlerInterceptor
{
    private static final Logger log = LoggerFactory.getLogger(VersionInterceptor.class);

    private static final AtomicReference<String> versionInfo = new AtomicReference<>();


    public VersionInterceptor()
    {
        try
        {
            versionInfo.set(IOUtils.toString(new ClassPathResource("/build_info.txt").getInputStream(), "UTF-8"));
        }
        catch (IOException e)
        {
            versionInfo.set("");
            log.error("", e);
        }
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        request.setAttribute("version_info", versionInfo.get());
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception
    {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception
    {

    }
}
