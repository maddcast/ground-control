package com.deploist.groundcontrol.spring;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 12/08/16
 * Time: 23:15
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
    @Override
    protected void configure(HttpSecurity http) throws Exception
    {
        http.authorizeRequests()
//                .antMatchers("/api-endpoint/*")
//                .permitAll()
                .anyRequest().authenticated();

        // auth
        http.httpBasic();
//                .formLogin()
//                .loginPage("/login")
//                .permitAll()

        // csrf
        http.csrf().disable();
//        http.csrf().ignoringAntMatchers("/api-endpoint/*");

        // logout
        http.logout()
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/");
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception
    {
        auth
                .userDetailsService(username -> {
                    if ("maddcast".equals(username))
                    {
                        return new User(username, "dfefd72e94dfc735270b900ed32b02aa", Lists.newArrayList(new SimpleGrantedAuthority("ADMIN")));
                    }
                    else
                    {
                        return null;
                    }
                })
                .passwordEncoder(new Md5PasswordEncoder());
    }
}
