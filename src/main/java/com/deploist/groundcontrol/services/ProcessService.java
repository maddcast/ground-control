package com.deploist.groundcontrol.services;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.domain.Version;
import com.deploist.groundcontrol.util.ParamsUtils;
import com.deploist.groundcontrol.util.Starter;
import com.deploist.groundcontrol.util.StringUtilsEx;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.io.SequenceInputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by maddcast.
 */
@Service
public class ProcessService
{
    private static final Logger log = LoggerFactory.getLogger(ProcessService.class);

    @Resource
    private VersionService versionService;


    public Starter getStarter(Distribution dist, String version)
    {
        return new Starter(dist, version);
    }


    public String runAndWait(Distribution dist, String paramKey) throws IOException
    {
        return run(dist, paramKey, true);
    }


    public String run(Distribution dist, String paramKey) throws IOException
    {
        return run(dist, paramKey, false);
    }


    public String run(Distribution dist, String paramKey, boolean wait) throws IOException
    {
        List<Version> versions = versionService.getVersions(dist);
        Map<String, String> params = ParamsUtils.createParams(dist, versionService.getCurrentVersion(versions, null));
        List<String> cmd = StringUtilsEx.parseCommandline(params.get(paramKey));

        File file = new File(cmd.get(0));
        if (!file.exists())
        {
            return "File " + file.getAbsolutePath() + " not found";
        }
        if (!file.canExecute())
        {
            return "File " + file.getAbsolutePath() + " is not executable";
        }
        ProcessBuilder processBuilder = new ProcessBuilder(cmd);
        processBuilder.directory(file.getParentFile());
        log.info("Starting process {}", processBuilder.command());
        Process process = processBuilder.start();
        try
        {
            if (wait)
            {
                process.waitFor();
            }
            else
            {
                process.waitFor(3, TimeUnit.SECONDS);
            }
        }
        catch (InterruptedException e)
        {
            log.error("", e);
        }

        return IOUtils.toString(new SequenceInputStream(process.getInputStream(), process.getErrorStream()), "UTF-8");
    }
}
