package com.deploist.groundcontrol.services;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.tasks.GroovyDeploy;
import com.google.common.io.Files;
import groovy.lang.GroovyShell;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Created by maddcast.
 */
@Service
public class GroovyDeployService
{
    @Resource
    private VersionService versionService;
    @Resource
    private ProcessService processService;


    private File getDeployScriptFile(String projectId)
    {
        return new File("config/" + projectId + ".groovy");
    }


    public boolean isDeployScriptExists(String projectId)
    {
        return getDeployScriptFile(projectId).exists();
    }


    public String getDeployScript(String projectId) throws IOException
    {
        File file = getDeployScriptFile(projectId);
        if (file.exists())
        {
            return Files.toString(file, Charset.forName("UTF-8"));
        }
        return null;
    }


    public void setDeployScript(String projectId, String groovy) throws IOException
    {
        File file = getDeployScriptFile(projectId);
        Files.write(groovy.getBytes("UTF-8"), file);
    }


    public void deployArtifact(Distribution dist, File artifactFile, File deployDir) throws IOException
    {
        CompilerConfiguration compilerConfiguration = new CompilerConfiguration();
        compilerConfiguration.setScriptBaseClass(GroovyDeploy.class.getName());
        GroovyShell groovyShell = new GroovyShell(compilerConfiguration);

        groovyShell.setVariable("dist", dist);
        groovyShell.setVariable("artifactFile", artifactFile);
        groovyShell.setVariable("deployDir", deployDir);
        groovyShell.setVariable("versionService", versionService);
        groovyShell.setVariable("processService", processService);

        groovyShell.parse(getDeployScriptFile(dist.getProjectId())).run();
    }
}
