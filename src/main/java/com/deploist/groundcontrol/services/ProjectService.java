package com.deploist.groundcontrol.services;

import com.deploist.groundcontrol.dao.ProjectDao;
import com.deploist.groundcontrol.domain.Project;
import com.deploist.groundcontrol.util.ProjectFinder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 23:11
 */
@Service
public class ProjectService
{
    @Resource
    private ProjectDao projectDao;


    public Project getProjectById(String id)
    {
        Project root = projectDao.getTree();

        if (StringUtils.isBlank(id))
        {
            return root;
        }

        return ProjectFinder.find(root, project -> id.equals(project.getId()));
    }


    public List<Project> getProjectParents(Project project)
    {
        List<Project> parents = new ArrayList<>();
        
        Project child = project;

        while ((child != null) && StringUtils.isNotBlank(child.getParentId()))
        {
            Project parent = getProjectById(child.getParentId());
            if (parent != null)
            {
                parents.add(0, parent);
            }
            child = parent;
        }

        return parents;
    }


    public Project createProject(String name, String parentId)
    {
        Project project = new Project();


        project.setId(UUID.randomUUID().toString());
        project.setName(name);

        Project parent = getProjectById(parentId);
        project.setParentId(parent.getId());
        parent.getProjects().add(project);
        parent.getProjects().sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));

        return project;
    }


    public void saveAllChanges() throws IOException
    {
        projectDao.saveTree();
    }
}
