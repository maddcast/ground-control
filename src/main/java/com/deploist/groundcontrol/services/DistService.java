package com.deploist.groundcontrol.services;

import com.deploist.groundcontrol.dao.DistDao;
import com.deploist.groundcontrol.domain.Distribution;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 23:11
 */
@Service
public class DistService
{
    @Resource
    private DistDao distDao;


    public Distribution getByProject(String projectId)
    {
        return projectId != null ? distDao.getAll().get(projectId) : null;
    }


    public Distribution createDistribution(String projectId)
    {
        Distribution dist = new Distribution();


        dist.setId(UUID.randomUUID().toString());
        dist.setProjectId(projectId);

        distDao.getAll().put(projectId, dist);

        return dist;
    }


    public Map<String, Distribution> getAll()
    {
        return distDao.getAll();
    }


    public void saveAllChanges() throws IOException
    {
        distDao.saveAll();
    }
}
