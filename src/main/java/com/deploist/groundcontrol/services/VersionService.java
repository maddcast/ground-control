package com.deploist.groundcontrol.services;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.domain.Version;
import com.deploist.groundcontrol.util.ParamNames;
import com.deploist.groundcontrol.util.ParamsUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 20/08/16
 * Time: 17:54
 */
@Service
public class VersionService
{
    private static final Logger log = LoggerFactory.getLogger(VersionService.class);


    public List<Version> getVersions(Distribution dist)
    {
        List<Version> versions = new ArrayList<>();

        Map<String, String> params = ParamsUtils.createParams(dist, ParamNames.NO_VERSION);

        if (params.containsKey(ParamNames.DEPLOY_DIR))
        {
            File deployDir = new File(params.get(ParamNames.DEPLOY_DIR));

            if (deployDir.exists())
            {
                File[] dirs = deployDir.listFiles(file -> {
                    try
                    {
                        return file.isDirectory() && !FileUtils.isSymlink(file);
                    }
                    catch (IOException e)
                    {
                        log.error("", e);
                        return false;
                    }
                });
                Path versionLink = deployDir.toPath().resolve("current");
                String currentVersion = null;
                if (Files.exists(versionLink) && Files.isSymbolicLink(versionLink) && Files.isDirectory(versionLink))
                {
                    try
                    {
                        currentVersion = deployDir.getAbsolutePath() + File.separator + Files.readSymbolicLink(versionLink).toString();
                    }
                    catch (IOException e)
                    {
                        log.error("", e);
                    }
                }

                for (File dir : dirs)
                {
                    Version version = new Version();
                    version.setNumber(dir.getName());
                    version.setDeployDate(new Date(dir.lastModified()));
                    version.setDeployDir(dir.getAbsolutePath());
                    version.setCurrent((currentVersion != null) && (currentVersion.equals(version.getDeployDir())));
                    versions.add(version);
                }

                versions.sort((o1, o2) -> (int) (o1.getDeployDate().getTime() - o2.getDeployDate().getTime()));
            }
        }

        return versions;
    }


    public String getCurrentVersion(List<Version> versions, String defaultVersion)
    {
        for (Version version : versions)
        {
            if (version.isCurrent())
            {
                return version.getNumber();
            }
        }

        return defaultVersion;
    }


    public File getVersionUploadDir(Distribution dist, String versionNumber) throws IOException
    {
        if (StringUtils.isBlank(versionNumber))
        {
            versionNumber = new SimpleDateFormat("dd.MM.yyyy-HH.mm").format(new Date());
        }
        Map<String, String> params = ParamsUtils.createParams(dist, versionNumber);
        File deployDir;
        String versionDirName = params.get(ParamNames.VERSION_DIR_NAME);
        if (StringUtils.isBlank(versionDirName))
        {
            deployDir = new File(params.get(ParamNames.DEPLOY_DIR));
        }
        else
        {
            deployDir = new File(params.get(ParamNames.DEPLOY_DIR) + File.separator + versionDirName);
        }

        if (!deployDir.exists())
        {
            if (!deployDir.mkdirs())
            {
                throw new IOException("Can not create dir " + deployDir.getAbsolutePath());
            }
        }

        return deployDir;
    }


    public void changeVersion(Distribution dist, String newVersionNumber) throws IOException
    {
        Map<String, String> params = ParamsUtils.createParams(dist, ParamNames.NO_VERSION);
        File deployDir = new File(params.get(ParamNames.DEPLOY_DIR));

        if (!deployDir.exists())
        {
            if (!deployDir.mkdirs())
            {
                throw new IOException("Can not create dir " + deployDir.getAbsolutePath());
            }
        }

        Path linkPath = Paths.get(deployDir.getAbsolutePath(), "current");

        Files.deleteIfExists(linkPath);
        Files.createSymbolicLink(linkPath, Paths.get(newVersionNumber));
    }
}
