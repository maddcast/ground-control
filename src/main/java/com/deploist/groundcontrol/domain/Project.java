package com.deploist.groundcontrol.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maddcast.
 */
public class Project
{
    private String id;
    private String parentId;
    private String name;
    private boolean disabled;
    private List<Project> projects = new ArrayList<>();


    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isDisabled()
    {
        return disabled;
    }

    public void setDisabled(boolean disabled)
    {
        this.disabled = disabled;
    }

    public List<Project> getProjects()
    {
        return projects;
    }

    public void setProjects(List<Project> projects)
    {
        this.projects = projects;
    }
}
