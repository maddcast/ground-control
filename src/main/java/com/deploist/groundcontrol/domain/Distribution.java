package com.deploist.groundcontrol.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by maddcast.
 */
public class Distribution
{
    private String id;
    private String projectId;
    private boolean hasIcon;
    private Map<String, String> params = new HashMap<>();


    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public boolean isHasIcon()
    {
        return hasIcon;
    }

    public void setHasIcon(boolean hasIcon)
    {
        this.hasIcon = hasIcon;
    }

    public Map<String, String> getParams()
    {
        return params;
    }

    public void setParams(Map<String, String> params)
    {
        this.params = params;
    }
}
