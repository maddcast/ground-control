package com.deploist.groundcontrol.domain;

import java.util.Date;

/**
 * Created by maddcast.
 */
public class Version
{
    private String number;
    private Date deployDate;
    private String deployDir;
    private boolean current;


    public String getNumber()
    {
        return number;
    }

    public void setNumber(String number)
    {
        this.number = number;
    }

    public Date getDeployDate()
    {
        return deployDate;
    }

    public void setDeployDate(Date deployDate)
    {
        this.deployDate = deployDate;
    }

    public String getDeployDir()
    {
        return deployDir;
    }

    public void setDeployDir(String deployDir)
    {
        this.deployDir = deployDir;
    }

    public boolean isCurrent()
    {
        return current;
    }

    public void setCurrent(boolean current)
    {
        this.current = current;
    }
}
