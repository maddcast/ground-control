package com.deploist.groundcontrol.controllers;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 26/08/16
 * Time: 00:20
 */
@Controller
public class IconController
{
    @RequestMapping(value = "/icon/{projectId}", produces = "image/png")
    @ResponseBody
    public byte[] view(@PathVariable String projectId) throws IOException
    {
        return FileUtils.readFileToByteArray(new File("config/icons/" + projectId + ".png"));
    }
}
