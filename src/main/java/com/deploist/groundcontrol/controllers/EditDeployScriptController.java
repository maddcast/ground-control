package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.models.EditDeployScriptModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.GroovyDeployService;
import com.deploist.groundcontrol.util.RedirectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 21:13
 */
@Controller
public class EditDeployScriptController
{
    private static final Logger log = LoggerFactory.getLogger(EditDeployScriptController.class);

    @Resource
    private DistService distService;
    @Resource
    private GroovyDeployService groovyDeployService;


    @RequestMapping(value = "/dists/script/edit", method = RequestMethod.GET)
    public String prepareEdit(@ModelAttribute("editDeployScriptModel") EditDeployScriptModel editDeployScriptModel) throws IOException
    {
        Distribution dist = distService.getByProject(editDeployScriptModel.getProjectId());

        editDeployScriptModel.setScript(groovyDeployService.getDeployScript(dist.getProjectId()));

        return "dists.script.edit";
    }


    @RequestMapping(value = "/dists/script/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("editDeployScriptModel") EditDeployScriptModel editDeployScriptModel) throws IOException
    {
        groovyDeployService.setDeployScript(editDeployScriptModel.getProjectId(), editDeployScriptModel.getScript());

        return "redirect:" + RedirectUtils.getProjectListUri(editDeployScriptModel.getProjectId());
    }
}
