package com.deploist.groundcontrol.controllers.api;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.models.VersionUploadModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.GroovyDeployService;
import com.deploist.groundcontrol.services.VersionService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * Created by maddcast.
 */
@Controller
public class ApiVersionUploadController
{
    @Resource
    private DistService distService;
    @Resource
    private VersionService versionService;
    @Resource
    private GroovyDeployService groovyDeployService;


    @RequestMapping(value = "/api-endpoint/versions/upload/zip", method = RequestMethod.POST)
    @ResponseBody
    public String uploadZip(@ModelAttribute("versionUploadModel") VersionUploadModel versionUploadModel) throws IOException
    {
        Distribution dist = distService.getByProject(versionUploadModel.getProjectId());

        MultipartFile versionFile = versionUploadModel.getVersionFile();
        if ((versionFile != null) && (!versionFile.isEmpty()))
        {
            File deployDir = versionService.getVersionUploadDir(dist, versionUploadModel.getVersionNumber());

            File artifactFile = new File(deployDir.getAbsolutePath() + File.separator + FilenameUtils.getName(versionFile.getOriginalFilename()));
            File partFile = new File(artifactFile.getAbsolutePath() + ".part");
            FileUtils.copyInputStreamToFile(versionFile.getInputStream(), partFile);
            partFile.renameTo(artifactFile);

            if (groovyDeployService.isDeployScriptExists(versionUploadModel.getProjectId()))
            {
                groovyDeployService.deployArtifact(dist, artifactFile, deployDir);
            }
        }

        versionUploadModel.setVersions(versionService.getVersions(dist));

        return "Thanks";
    }
}
