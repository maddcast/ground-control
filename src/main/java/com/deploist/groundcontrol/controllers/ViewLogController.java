package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.domain.Version;
import com.deploist.groundcontrol.models.LogModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.VersionService;
import com.deploist.groundcontrol.util.ParamNames;
import com.deploist.groundcontrol.util.ParamsUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 21/08/16
 * Time: 22:19
 */
@Controller
public class ViewLogController
{
    @Resource
    private DistService distService;
    @Resource
    private VersionService versionService;


    @RequestMapping("/log")
    public String view(@ModelAttribute("logModel") LogModel logModel) throws IOException
    {
        Distribution dist = distService.getByProject(logModel.getProjectId());
        List<Version> versions = versionService.getVersions(dist);
        Map<String, String> params = ParamsUtils.createParams(dist, versionService.getCurrentVersion(versions, null));
        String logPath = params.get(ParamNames.LOG_PATH);

        int logSize = 8192;

        try (RandomAccessFile file = new RandomAccessFile(logPath, "r"))
        {
            if (file.length() > logSize)
            {
                file.seek(file.length() - logSize);
            }

            byte[] buf = new byte[logSize];

            int len = file.read(buf);

            logModel.setLog(new String(buf, 0, len, "UTF-8"));
        }

        return "logs.view";
    }
}
