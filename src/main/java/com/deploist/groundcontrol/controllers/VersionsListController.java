package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.models.VersionsListModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.VersionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created by maddcast.
 */
@Controller
@Deprecated
public class VersionsListController
{
    @Resource
    private DistService distService;
    @Resource
    private VersionService versionService;


    @RequestMapping("/versions/list")
    public String list(@ModelAttribute("versionsListModel") VersionsListModel versionsListModel)
    {
        Distribution dist = distService.getByProject(versionsListModel.getProjectId());
        versionsListModel.setVersions(versionService.getVersions(dist));

        return "versions.list";
    }
}
