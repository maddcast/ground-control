package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.models.ProjectTreeModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.ProjectService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 17:51
 */
@Controller
public class ProjectsTreeController
{
    @Resource
    private ProjectService projectService;
    @Resource
    private DistService distService;


    @RequestMapping("/projects/tree")
    public String tree(@ModelAttribute("projectTreeModel") ProjectTreeModel projectTreeModel)
    {
        projectTreeModel.setTree(projectService.getProjectById(null));
        projectTreeModel.setDists(distService.getAll());

        return "projects.tree";
    }
}
