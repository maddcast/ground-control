package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.domain.Version;
import com.deploist.groundcontrol.models.VersionSwitchModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.ProcessService;
import com.deploist.groundcontrol.services.VersionService;
import com.deploist.groundcontrol.util.ParamNames;
import com.deploist.groundcontrol.util.RedirectUtils;
import com.deploist.groundcontrol.util.Starter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 21/08/16
 * Time: 18:06
 */
@Controller
public class VersionSwitchController
{
    @Resource
    private DistService distService;
    @Resource
    private VersionService versionService;
    @Resource
    private ProcessService processService;


    @RequestMapping(value = "/versions/list", method = RequestMethod.GET)
    public String list(@ModelAttribute("versionSwitchModel") VersionSwitchModel versionSwitchModel)
    {
        Distribution dist = distService.getByProject(versionSwitchModel.getProjectId());
        List<Version> versions = versionService.getVersions(dist);
        versionSwitchModel.setVersions(versions);
        versionSwitchModel.setCurrentVersion(versionService.getCurrentVersion(versionService.getVersions(dist), null));

        return "versions.switch";
    }


    @RequestMapping(value = "/versions/switch", method = RequestMethod.POST)
    public String makeSwitch(@ModelAttribute("versionSwitchModel") VersionSwitchModel versionSwitchModel) throws IOException
    {
        Distribution dist = distService.getByProject(versionSwitchModel.getProjectId());
        Starter starter = processService.getStarter(dist, null);
        if (versionSwitchModel.isRestart() && (starter.canStop()) && (!starter.canRestart()))
        {
            processService.runAndWait(dist, ParamNames.STOP_PATH);
        }
        versionService.changeVersion(dist, versionSwitchModel.getCurrentVersion());
        if (versionSwitchModel.isRestart())
        {
            if (starter.canRestart())
            {
                processService.run(dist, ParamNames.RESTART_PATH);
            }
            else
            {
                processService.run(dist, ParamNames.START_PATH);
            }
        }

        return "redirect:" + RedirectUtils.getVersionListUri(versionSwitchModel.getProjectId());
    }
}
