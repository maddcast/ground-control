package com.deploist.groundcontrol.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 11:14
 */
@Controller
public class IndexController
{
    @RequestMapping("/")
    public String index()
    {
        return "index";
    }
}
