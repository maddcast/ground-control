package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.models.VersionUploadModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.GroovyDeployService;
import com.deploist.groundcontrol.services.VersionService;
import com.deploist.groundcontrol.tasks.deploy.GzipTask;
import com.deploist.groundcontrol.tasks.deploy.UnzipTask;
import com.deploist.groundcontrol.util.RedirectUtils;
import com.deploist.groundcontrol.util.downloader.Downloader;
import com.deploist.groundcontrol.util.downloader.QueueFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
 * Created by maddcast.
 */
@Controller
public class VersionUploadController
{
    @Resource
    private DistService distService;
    @Resource
    private VersionService versionService;
    @Resource
    private GroovyDeployService groovyDeployService;


    @RequestMapping(value = "/versions/pre_upload", method = RequestMethod.GET)
    public String prepareUpload(@ModelAttribute("versionUploadModel") VersionUploadModel versionUploadModel)
    {
        Distribution dist = distService.getByProject(versionUploadModel.getProjectId());
        versionUploadModel.setVersions(versionService.getVersions(dist));

        return "versions.upload";
    }


    @RequestMapping(value = "/versions/upload/zip", method = RequestMethod.POST)
    public String uploadZip(@ModelAttribute("versionUploadModel") VersionUploadModel versionUploadModel) throws IOException
    {
        Distribution dist = distService.getByProject(versionUploadModel.getProjectId());

        MultipartFile versionFile = versionUploadModel.getVersionFile();
        if ((versionFile != null) && (!versionFile.isEmpty()))
        {
            File deployDir = versionService.getVersionUploadDir(dist, versionUploadModel.getVersionNumber());

            File artifactFile = new File(deployDir.getAbsolutePath() + File.separator + FilenameUtils.getName(versionFile.getOriginalFilename()));
            File partFile = new File(artifactFile.getAbsolutePath() + ".part");
            FileUtils.copyInputStreamToFile(versionFile.getInputStream(), partFile);
            partFile.renameTo(artifactFile);

            if (groovyDeployService.isDeployScriptExists(versionUploadModel.getProjectId()))
            {
                groovyDeployService.deployArtifact(dist, artifactFile, deployDir);
            }
            else
            {
                String resultDir = artifactFile.getParent();
                if (versionUploadModel.isExtractArchive())
                {
                    new UnzipTask().work(artifactFile);
                }
                if (versionUploadModel.isDeflateFiles())
                {
                    new GzipTask().work(new File(resultDir));
                }
            }
        }

        versionUploadModel.setVersions(versionService.getVersions(dist));

        return "redirect:" + RedirectUtils.getVersionListUri(versionUploadModel.getProjectId());
    }


    @RequestMapping(value = "/versions/upload/teamcity_artifact", method = RequestMethod.POST)
    public String uploadTeamcityArtifact(@ModelAttribute("versionUploadModel") VersionUploadModel versionUploadModel) throws IOException
    {
        Distribution dist = distService.getByProject(versionUploadModel.getProjectId());

        String artifactUrl = versionUploadModel.getTeamcityArtifactUrl();
        if ((!StringUtils.startsWith(artifactUrl, "http://")) && (!StringUtils.startsWith(artifactUrl, "https://")))
        {
            artifactUrl = "http://" + artifactUrl;
        }

        File deployDir = versionService.getVersionUploadDir(dist, versionUploadModel.getVersionNumber());

        Downloader downloader = new Downloader();
        QueueFile queueFile = new QueueFile();
        queueFile.setUrl(artifactUrl);
        queueFile.setOutputDir(deployDir.getAbsolutePath());
        queueFile.setFilename(StringUtils.substringAfterLast(artifactUrl, "/"));
        queueFile.setUsername(versionUploadModel.getTeamcityUsername());
        queueFile.setPassword(versionUploadModel.getTeamcityPassword());
        downloader.queue(queueFile);
        downloader.download();

        if (groovyDeployService.isDeployScriptExists(versionUploadModel.getProjectId()))
        {
            groovyDeployService.deployArtifact(dist, queueFile.getResultFile(), deployDir);
        }
        else
        {
            String resultDir = queueFile.getResultFile().getParent();
            if (versionUploadModel.isExtractArchive())
            {
                new UnzipTask().work(queueFile.getResultFile());
            }
            if (versionUploadModel.isDeflateFiles())
            {
                new GzipTask().work(new File(resultDir));
            }
        }

        versionUploadModel.setVersions(versionService.getVersions(dist));

        return "redirect:" + RedirectUtils.getVersionListUri(versionUploadModel.getProjectId());
    }
}
