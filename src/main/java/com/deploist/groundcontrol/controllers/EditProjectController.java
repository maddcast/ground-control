package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.domain.Project;
import com.deploist.groundcontrol.models.EditProjectModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.ProjectService;
import com.deploist.groundcontrol.util.RedirectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 21:13
 */
@Controller
public class EditProjectController
{
    @Resource
    private ProjectService projectService;
    @Resource
    private DistService distService;


    @RequestMapping(value = "/projects/add", method = RequestMethod.GET)
    public String prepareAdd(@ModelAttribute("editProjectModel") EditProjectModel editProjectModel)
    {
        return "projects.edit";
    }


    @RequestMapping(value = "/projects/edit/{id}", method = RequestMethod.GET)
    public String prepareEdit(@PathVariable String id, @ModelAttribute("editProjectModel") EditProjectModel editProjectModel)
    {
        Project project = projectService.getProjectById(id);

        editProjectModel.setId(project.getId());
        editProjectModel.setParentId(project.getParentId());
        editProjectModel.setName(project.getName());
        editProjectModel.setDist(distService.getByProject(project.getId()));

        return "projects.edit";
    }


    @RequestMapping(value = "/projects/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("editProjectModel") EditProjectModel editProjectModel) throws IOException
    {
        Project project;

        if (StringUtils.isBlank(editProjectModel.getId()))
        {
            projectService.createProject(editProjectModel.getName(), editProjectModel.getParentId());
        }
        else
        {
            project = projectService.getProjectById(editProjectModel.getId());
            project.setName(editProjectModel.getName());
        }

        projectService.saveAllChanges();

        return "redirect:" + RedirectUtils.getProjectListUri(editProjectModel.getParentId());
    }
}
