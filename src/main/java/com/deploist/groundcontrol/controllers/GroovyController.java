package com.deploist.groundcontrol.controllers;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 19/08/16
 * Time: 08:53
 */
@Controller
public class GroovyController
{
    private static final Logger log = LoggerFactory.getLogger(GroovyController.class);


    @RequestMapping("/groovy/{scriptName}")
    @ResponseBody
    public String run(@PathVariable String scriptName)
    {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("groovy");
        StringWriter responseWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(responseWriter);
        engine.getContext().setWriter(writer);
        engine.getContext().setErrorWriter(writer);

        try (FileReader scriptFileReader = new FileReader(new File("groovy/" + scriptName + ".groovy")))
        {
            engine.eval(scriptFileReader);
            return responseWriter.toString();
        }
        catch (Exception e)
        {
            log.error("", e);
            return ExceptionUtils.getStackTrace(e);
        }
    }
}
