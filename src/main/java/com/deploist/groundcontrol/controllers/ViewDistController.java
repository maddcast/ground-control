package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.models.FormFileParam;
import com.deploist.groundcontrol.models.ViewDistModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.ProcessService;
import com.deploist.groundcontrol.services.ProjectService;
import com.deploist.groundcontrol.services.VersionService;
import com.deploist.groundcontrol.util.ParamNames;
import com.deploist.groundcontrol.util.ParamsUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 21:13
 */
@Controller
public class ViewDistController
{
    @Resource
    private ProjectService projectService;
    @Resource
    private DistService distService;
    @Resource
    private VersionService versionService;
    @Resource
    private ProcessService processService;


    @RequestMapping(value = "/dists/view", method = RequestMethod.GET)
    public String prepareEdit(@ModelAttribute("viewDistModel") ViewDistModel viewDistModel)
    {
        Distribution dist = distService.getByProject(viewDistModel.getProjectId());

        viewDistModel.setId(dist.getId());
        viewDistModel.setProjectId(dist.getProjectId());
        viewDistModel.setProject(projectService.getProjectById(dist.getProjectId()));
        viewDistModel.setHasIcon(dist.isHasIcon());

        viewDistModel.setVersions(versionService.getVersions(dist));
        String currentVersion = versionService.getCurrentVersion(viewDistModel.getVersions(), ParamNames.NO_VERSION);
        viewDistModel.setStarter(processService.getStarter(dist, currentVersion));

        Map<String, String> params = new HashMap<>(dist.getParams());
        List<FormFileParam> fileParams = new ArrayList<>();
        fileParams.add(getFileValue(params, ParamNames.BASE_DIR, currentVersion));
        fileParams.add(getFileValue(params, ParamNames.DEPLOY_DIR, currentVersion));
        fileParams.add(getFileValue(params, ParamNames.LOG_PATH, currentVersion));
        fileParams.add(getFileValue(params, ParamNames.START_PATH, currentVersion));
        fileParams.add(getFileValue(params, ParamNames.STOP_PATH, currentVersion));
        fileParams.add(getFileValue(params, ParamNames.RESTART_PATH, currentVersion));
        fileParams.add(getFileValue(params, ParamNames.PID_PATH, currentVersion));
        viewDistModel.setFileParams(fileParams);
        viewDistModel.setVersionDirName(getParamValue(params, ParamNames.VERSION_DIR_NAME, currentVersion));

        return "dists.view";
    }


    private FormFileParam getFileValue(Map<String, String> params, String key, String currentVersion)
    {
        return new FormFileParam(key, getParamValue(params, key, ParamNames.NO_VERSION), getParamValue(params, key, currentVersion));
    }


    private String getParamValue(Map<String, String> params, String key, String version)
    {
        return ParamsUtils.getParamValue(params, key, version);
    }
}
