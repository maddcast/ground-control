package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.domain.Project;
import com.deploist.groundcontrol.models.ProjectListModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.ProjectService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 17:51
 */
@Controller
public class ProjectsListController
{
    @Resource
    private ProjectService projectService;
    @Resource
    private DistService distService;


    @RequestMapping("/projects/list")
    public String list(@RequestParam(required = false) String parentId,
                       @ModelAttribute("projectListModel") ProjectListModel projectListModel)
    {
        projectListModel.setParentId(parentId);
        Project project = projectService.getProjectById(parentId);
        if (project != null)
        {
            projectListModel.setCurrentProject(project);
            projectListModel.setProjectParents(projectService.getProjectParents(project));
            projectListModel.setProjects(project.getProjects());
            projectListModel.setDist(distService.getByProject(project.getId()));
        }

        return "projects.list";
    }
}
