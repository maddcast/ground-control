package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.models.EditDistModel;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.util.ParamNames;
import com.deploist.groundcontrol.util.RedirectUtils;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 21:13
 */
@Controller
public class EditDistController
{
    private static final Logger log = LoggerFactory.getLogger(EditDistController.class);

    @Resource
    private DistService distService;


    @RequestMapping(value = "/dists/add", method = RequestMethod.GET)
    public String prepareAdd(@ModelAttribute("editDistModel") EditDistModel editDistModel)
    {
        return "dists.edit";
    }


    @RequestMapping(value = "/dists/edit", method = RequestMethod.GET)
    public String prepareEdit(@ModelAttribute("editDistModel") EditDistModel editDistModel)
    {
        Distribution dist = distService.getByProject(editDistModel.getProjectId());

        editDistModel.setId(dist.getId());
        editDistModel.setProjectId(dist.getProjectId());
        editDistModel.setBasePath(dist.getParams().get(ParamNames.BASE_DIR));
        editDistModel.setDeployPath(dist.getParams().get(ParamNames.DEPLOY_DIR));
        editDistModel.setVersionDirName(dist.getParams().get(ParamNames.VERSION_DIR_NAME));
        editDistModel.setLogPath(dist.getParams().get(ParamNames.LOG_PATH));
        editDistModel.setStartPath(dist.getParams().get(ParamNames.START_PATH));
        editDistModel.setStopPath(dist.getParams().get(ParamNames.STOP_PATH));
        editDistModel.setRestartPath(dist.getParams().get(ParamNames.RESTART_PATH));
        editDistModel.setPidPath(dist.getParams().get(ParamNames.PID_PATH));

        return "dists.edit";
    }


    @RequestMapping(value = "/dists/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("editDistModel") EditDistModel editDistModel) throws IOException
    {
        Distribution dist = distService.getByProject(editDistModel.getProjectId());

        if (dist == null)
        {
            dist = distService.createDistribution(editDistModel.getProjectId());
        }

        put(dist, ParamNames.BASE_DIR, editDistModel.getBasePath());
        put(dist, ParamNames.DEPLOY_DIR, editDistModel.getDeployPath());
        put(dist, ParamNames.VERSION_DIR_NAME, editDistModel.getVersionDirName());
        put(dist, ParamNames.LOG_PATH, editDistModel.getLogPath());
        put(dist, ParamNames.START_PATH, editDistModel.getStartPath());
        put(dist, ParamNames.STOP_PATH, editDistModel.getStopPath());
        put(dist, ParamNames.RESTART_PATH, editDistModel.getRestartPath());
        put(dist, ParamNames.PID_PATH, editDistModel.getPidPath());

        if ((editDistModel.getIconFile() != null) && (!editDistModel.getIconFile().isEmpty()))
        {
            String filename = "config/icons" + File.separator + dist.getProjectId() + ".png";
            File iconFile = new File(filename);
            File iconDir = iconFile.getParentFile();
            if (!iconDir.exists())
            {
                if (!iconDir.mkdirs())
                {
                    log.error("Error creating dir " + iconDir.getAbsolutePath());
                }
            }
            BufferedImage icon = ImageIO.read(editDistModel.getIconFile().getInputStream());
            BufferedImage croppedImage = Scalr.crop(icon, Math.min(icon.getWidth(), icon.getHeight()), Math.min(icon.getWidth(), icon.getHeight()));
            BufferedImage newIcon = Scalr.resize(croppedImage, Scalr.Method.ULTRA_QUALITY, Scalr.Mode.FIT_EXACT, 100);
            ImageIO.write(newIcon, "png", iconFile);
            dist.setHasIcon(true);
        }

        distService.saveAllChanges();

        return "redirect:" + RedirectUtils.getProjectListUri(editDistModel.getProjectId());
    }


    private void put(Distribution dist, String key, String value)
    {
        if (value != null)
        {
            dist.getParams().put(key, value);
        }
    }
}
