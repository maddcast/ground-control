package com.deploist.groundcontrol.controllers;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.services.DistService;
import com.deploist.groundcontrol.services.ProcessService;
import com.deploist.groundcontrol.util.ParamNames;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * Created by maddcast.
 */
@Controller
public class RunProcessController
{
//    private static final Logger log = LoggerFactory.getLogger(RunProcessController.class);

    @Resource
    private DistService distService;
    @Resource
    private ProcessService processService;


    @RequestMapping("/process/start/{projectId}")
    @ResponseBody
    public String start(@PathVariable String projectId) throws IOException
    {
        Distribution dist = distService.getByProject(projectId);

        return processService.run(dist, ParamNames.START_PATH);
    }


    @RequestMapping("/process/stop/{projectId}")
    @ResponseBody
    public String stop(@PathVariable String projectId) throws IOException
    {
        Distribution dist = distService.getByProject(projectId);

        return processService.run(dist, ParamNames.STOP_PATH);
    }


    @RequestMapping("/process/restart/{projectId}")
    @ResponseBody
    public String restart(@PathVariable String projectId) throws IOException
    {
        Distribution dist = distService.getByProject(projectId);

        return processService.run(dist, ParamNames.RESTART_PATH);
    }
}
