package com.deploist.groundcontrol.util;

import org.apache.commons.lang3.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 14/08/16
 * Time: 12:41
 */
public class RedirectUtils
{
    public static String getProjectListUri(String projectParentId)
    {
        String uri = "/projects/list";
        if (StringUtils.isNotBlank(projectParentId))
        {
            uri += "?parentId=" + projectParentId;
        }
        return uri;
    }


    public static String getVersionListUri(String projectId)
    {
        return "/versions/list?projectId=" + projectId;
    }


    public static String getLogUri(String projectId)
    {
        return "/log?projectId=" + projectId;
    }
}
