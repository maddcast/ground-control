package com.deploist.groundcontrol.util;

import com.deploist.groundcontrol.domain.Distribution;

import java.io.File;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 23/08/16
 * Time: 00:18
 */
public class Starter
{
    private final File startPath;
    private final File stopPath;
    private final File restartPath;
    private final File logPath;


    public Starter(Distribution dist, String version)
    {
        Map<String, String> params = ParamsUtils.createParams(dist, version);

        startPath = createFileParam(params, ParamNames.START_PATH);
        stopPath = createFileParam(params, ParamNames.STOP_PATH);
        restartPath = createFileParam(params, ParamNames.RESTART_PATH);
        logPath = createFileParam(params, ParamNames.LOG_PATH);
    }


    private File createFileParam(Map<String, String> params, String paramKey)
    {
        return params.containsKey(paramKey) ? new File(StringUtilsEx.parseCommandline(params.get(paramKey)).get(0)) : null;
    }


    public boolean canStart()
    {
        return (startPath != null) && (startPath.exists()) && (startPath.canExecute());
    }


    public boolean canStop()
    {
        return (stopPath != null) && (stopPath.exists()) && (stopPath.canExecute());
    }


    public boolean canRestart()
    {
        return (restartPath != null) && (restartPath.exists()) && (restartPath.canExecute());
    }


    public boolean canReadLog()
    {
        return (logPath != null) && (logPath.exists()) && (logPath.canRead());
    }
}
