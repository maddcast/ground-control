package com.deploist.groundcontrol.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * This utility extracts files and directories of a standard zip file to
 * a destination directory.
 *
 * @author www.codejava.net
 */
public class UnzipUtility
{
    private static final Logger log = LoggerFactory.getLogger(UnzipUtility.class);

    /**
     * Size of the buffer to read/write data
     */
    private static final int BUFFER_SIZE = 4096;

    /**
     * Extracts a zip file specified by the zipFilePath to a directory specified by
     * destDirectory (will be created if does not exists)
     *
     * @param zipFilePath
     * @param destDirectory
     * @throws IOException
     */
    public void unzip(String zipFilePath, String destDirectory) throws IOException
    {
        String omitPrefix = findRedundantPrefix(zipFilePath);
        createDirs(zipFilePath, destDirectory, omitPrefix);

        try (ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath)))
        {
            ZipEntry entry = zipIn.getNextEntry();
            // iterates over entries in the zip file
            while (entry != null)
            {
                if (!entry.isDirectory())
                {
                    String entryName = entry.getName();
                    if (omitPrefix != null)
                    {
                        entryName = StringUtils.remove(entryName, omitPrefix);
                    }

                    // if the entry is a file, extracts it
                    extractFile(zipIn, destDirectory + File.separator + entryName);
                }
                zipIn.closeEntry();
                entry = zipIn.getNextEntry();
            }
        }
    }


    private String findRedundantPrefix(String zipFilePath) throws IOException
    {
        String prefix = null;

        try (ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath)))
        {
            ZipEntry entry = zipIn.getNextEntry();
            // iterates over entries in the zip file
            while (entry != null)
            {
                if (!entry.getName().contains("/"))
                {
                    return null;
                }

                String entryDir = StringUtils.substringBeforeLast(entry.getName(), "/") + "/";
                if ((prefix == null) || (entryDir.length() < prefix.length()))
                {
                    prefix = entryDir;
                }

                zipIn.closeEntry();
                entry = zipIn.getNextEntry();
            }
        }

        return prefix;
    }


    private void createDirs(String zipFilePath, String destDirectory, String omitPrefix) throws IOException
    {
        try (ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath)))
        {
            ZipEntry entry = zipIn.getNextEntry();
            // iterates over entries in the zip file
            while (entry != null)
            {
                if (entry.isDirectory())
                {
                    // if the entry is a directory, make the directory
                    String entryName = entry.getName();
                    if (omitPrefix != null)
                    {
                        entryName = StringUtils.remove(entryName, omitPrefix);
                    }
                    File dir = new File(destDirectory + File.separator + entryName);
                    if (dir.mkdirs())
                    {
                        log.info("Created dir {}", dir.getAbsolutePath());
                    }
                }
                zipIn.closeEntry();
                entry = zipIn.getNextEntry();
            }
        }
    }


    /**
     * Extracts a zip entry (file entry)
     *
     * @param zipIn
     * @param filePath
     * @throws IOException
     */
    private void extractFile(ZipInputStream zipIn, String filePath) throws IOException
    {
        log.info("Extracting file {}", filePath);

        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath)))
        {
            byte[] bytesIn = new byte[BUFFER_SIZE];
            int read;
            while ((read = zipIn.read(bytesIn)) != -1)
            {
                bos.write(bytesIn, 0, read);
            }
        }
    }
}