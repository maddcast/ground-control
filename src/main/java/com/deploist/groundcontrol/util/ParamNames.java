package com.deploist.groundcontrol.util;

/**
 * Created by maddcast.
 */
public interface ParamNames
{
    String USER_HOME = "user_home";
    String BASE_DIR = "base_path";
    String DEPLOY_DIR = "deploy_path";
    String VERSION_DIR_NAME = "version_dir_name";
    String LOG_PATH = "log_path";
    String START_PATH = "start_path";
    String STOP_PATH = "stop_path";
    String RESTART_PATH = "restart_path";
    String PID_PATH = "pid_path";
    String VERSION = "version";
    String NO_VERSION = "%version%";
}
