package com.deploist.groundcontrol.util;

import com.deploist.groundcontrol.domain.Distribution;
import org.stringtemplate.v4.ST;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by maddcast.
 */
public class ParamsUtils
{
    public static Map<String, String> createParams(Distribution dist, String versionNumber)
    {
//        if (StringUtils.isBlank(versionNumber) && (dist.getCurrentVersion() != null))
//        {
//            versionNumber = dist.getCurrentVersion().getNumber();
//        }

        Map<String, String> params = new HashMap<>(dist.getParams());

        getParamValue(params, ParamNames.BASE_DIR, versionNumber);
        getParamValue(params, ParamNames.DEPLOY_DIR, versionNumber);
        getParamValue(params, ParamNames.VERSION_DIR_NAME, versionNumber);
        getParamValue(params, ParamNames.LOG_PATH, versionNumber);
        getParamValue(params, ParamNames.START_PATH, versionNumber);
        getParamValue(params, ParamNames.STOP_PATH, versionNumber);
        getParamValue(params, ParamNames.RESTART_PATH, versionNumber);
        getParamValue(params, "pid_path", versionNumber);

        return params;
    }


    public static String getParamValue(Map<String, String> params, String key, String versionNumber)
    {
        if (params.containsKey(key))
        {
            ST st = new ST(params.get(key), '%', '%');
            st.add(ParamNames.USER_HOME, System.getProperty("user.home"));
            st.add(ParamNames.VERSION, versionNumber);
            for (Map.Entry<String, String> entry : params.entrySet())
            {
                st.add(entry.getKey(), entry.getValue());
            }

            params.put(key, st.render());
        }

        return params.get(key);
    }
}
