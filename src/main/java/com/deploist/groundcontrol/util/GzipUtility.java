package com.deploist.groundcontrol.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.zip.GZIPOutputStream;

/**
 * Created by maddcast.
 */
public class GzipUtility
{
    private static final Logger log = LoggerFactory.getLogger(GzipUtility.class);


    public void gzipDir(String dir) throws IOException
    {
        log.info("Starting gzip for dir {}", dir);

        Files.walkFileTree(Paths.get(dir), new SimpleFileVisitor<Path>()
        {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
            {
                byte[] buffer = new byte[1024];

                String filename = file.toFile().getAbsolutePath();
                log.info("Gzip for file {}", filename);

                try (FileInputStream in = new FileInputStream(filename);
                     GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(filename + ".gz")))
                {
                    int len;
                    while ((len = in.read(buffer)) > 0)
                    {
                        out.write(buffer, 0, len);
                    }
                }

                return super.visitFile(file, attrs);
            }
        });

        log.info("Finished gzip for dir {}", dir);
    }
}
