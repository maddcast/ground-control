package com.deploist.groundcontrol.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maddcast.
 */
public class StringUtilsEx
{
    public static String getFilenameFromUrl(String url)
    {
        String shortName = StringUtils.substringAfterLast(url, "/");
        return StringUtils.substringBefore(shortName, "?");
    }


    public static List<String> parseCommandline(String cmd)
    {
        List<String> tokensList = new ArrayList<>();
        boolean inQuotes = false;
        StringBuilder b = new StringBuilder();
        for (char c : cmd.toCharArray()) {
            switch (c) {
                case ' ':
                    if (inQuotes) {
                        b.append(c);
                    } else {
                        tokensList.add(b.toString());
                        b = new StringBuilder();
                    }
                    break;
                case '\"':
                    inQuotes = !inQuotes;
                default:
                    b.append(c);
                    break;
            }
        }
        tokensList.add(b.toString());

        return tokensList;
    }


    public static String joinSeamless(String str1, String str2)
    {
        if (str1.charAt(str1.length() - 1) == str2.charAt(0))
        {
            return str1 + str2.substring(1);
        }
        else
        {
            return str1 + str2;
        }
    }
}
