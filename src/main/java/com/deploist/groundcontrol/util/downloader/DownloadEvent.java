package com.deploist.groundcontrol.util.downloader;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 06/04/16
 * Time: 09:12
 */
public interface DownloadEvent
{
    DownloadState onBytesReceived(byte[] image, File outFile);
    void onFinished(DownloadState state);
}
