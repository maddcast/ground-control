package com.deploist.groundcontrol.util.downloader;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 08/04/16
 * Time: 00:00
 */
public enum DownloadState
{
    NONE,
    SAVED,
    CANCELED
}
