package com.deploist.groundcontrol.util.downloader;

import com.deploist.groundcontrol.util.StringUtilsEx;
import com.google.common.base.Throwables;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 09/08/15
 * Time: 14:51
 */
public class Downloader
{
    private static final Logger log = LoggerFactory.getLogger(Downloader.class);

    private List<QueueFile> queueFiles = new ArrayList<>();
    private AtomicInteger countdown;
    private Runnable onClose = null;


    public void queue(QueueFile queueFile)
    {
        queueFiles.add(queueFile);
    }


    public Downloader done(Runnable onClose)
    {
        this.onClose = onClose;
        return this;
    }


    public void download()
    {
        countdown = new AtomicInteger(queueFiles.size());

        queueFiles.forEach(this::get);

        if (onClose != null)
        {
            onClose.run();
        }
    }


    private void get(QueueFile queueFile)
    {
        String filename = (queueFile.getFilename() != null) ? queueFile.getFilename() : StringUtilsEx.getFilenameFromUrl(queueFile.getUrl());
        File downloadedFile = new File(queueFile.getOutputDir() + "/" + filename);

        if (downloadedFile.exists())
        {
            log.info("File {} exists. Skipping.", downloadedFile.getName());
            countdown.decrementAndGet();
            if (queueFile.getDownloadEvent() != null)
            {
                queueFile.getDownloadEvent().onFinished(DownloadState.SAVED);
            }
            return;
        }

        log.info("Downloading {}", queueFile.getUrl());
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        if (StringUtils.isNoneBlank(queueFile.getUsername(), queueFile.getPassword()))
        {
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
            credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(queueFile.getUsername(), queueFile.getPassword()));
            httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
        }
        try (CloseableHttpClient httpClient = httpClientBuilder.build())
        {
            HttpGet httpGet = new HttpGet(queueFile.getUrl());
            HttpResponse httpResponse = httpClient.execute(httpGet);
            DownloadState state = DownloadState.NONE;
            if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK)
            {
                try
                {
                    if (queueFile.getDownloadEvent() != null)
                    {
                        byte[] buf = EntityUtils.toByteArray(httpResponse.getEntity());
                        state = queueFile.getDownloadEvent().onBytesReceived(buf, downloadedFile);
                    }
                    if (state == DownloadState.NONE)
                    {
                        long length = httpResponse.getEntity().getContentLength();
                        InputStream entityStream = httpResponse.getEntity().getContent();

                        File partFile = new File(downloadedFile.getAbsolutePath() + ".part");

                        if (!partFile.getParentFile().exists())
                        {
                            if (!partFile.getParentFile().mkdirs())
                            {
                                throw new RuntimeException("Can not make dir");
                            }
                        }

                        try (FileOutputStream outStream = new FileOutputStream(partFile))
                        {
                            byte[] buffer = new byte[2048];
                            int count;
                            float totalReadBytes = 0;
                            float totalReadPercentage = 0;
                            long startTime = System.currentTimeMillis();
                            while ((count = entityStream.read(buffer)) != -1)
                            {
                                outStream.write(buffer, 0, count);
                                totalReadBytes += count;
                                float newPercentage = getPercentage(totalReadBytes, length);
                                if (newPercentage > totalReadPercentage)
                                {
                                    totalReadPercentage = newPercentage;
                                    log.info("Writing {}% estimated {} to {}", totalReadPercentage, getEstimatedTime(startTime, totalReadBytes, length), downloadedFile.getName());
                                }
                            }
                            outStream.flush();
                        }

                        if (partFile.renameTo(downloadedFile))
                        {
                            log.info("Renamed temp file to {}", downloadedFile.getName());
                            queueFile.setResultFile(downloadedFile);
                        }

                        state = DownloadState.SAVED;
                    }
                    EntityUtils.consume(httpResponse.getEntity());
                    log.info("Finished {}. {} in a queue", queueFile.getUrl(), countdown.get() - 1);
                }
                catch (IOException e)
                {
                    log.error("", e);
                }
            } else
            {
                log.error("Error status {} in {}", httpResponse.getStatusLine().getStatusCode(), queueFile.getUrl());
            }
            httpGet.releaseConnection();
            countdown.decrementAndGet();
            if (queueFile.getDownloadEvent() != null)
            {
                queueFile.getDownloadEvent().onFinished(state);
            }
        }
        catch (Exception e)
        {
            Throwables.propagate(e);
        }
    }


    private float getPercentage(float number, float total)
    {
        return ((int)((number / total) * 10000f)) / 100f;
    }


    private String getEstimatedTime(long startTime, float readLength, float totalLength)
    {
        long spentTime = System.currentTimeMillis() - startTime;
        long estimatedTime = (long)(spentTime * (totalLength - readLength) / readLength);

        LocalDateTime localDateTime = LocalDateTime.of(0, 1, 1, 0, 0, 0).plusSeconds(estimatedTime / 1000);

        return localDateTime.getHour() + ":" +
                localDateTime.getMinute() + ":" +
                localDateTime.getSecond();
    }
}
