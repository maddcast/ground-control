package com.deploist.groundcontrol.util.downloader;

import java.io.File;

/**
 * Created by maddcast.
 */
public class QueueFile
{
    private String outputDir;
    private String url;
    private String filename;
    private String username;
    private String password;
    private DownloadEvent downloadEvent;
    private File resultFile;


    public String getOutputDir()
    {
        return outputDir;
    }

    public void setOutputDir(String outputDir)
    {
        this.outputDir = outputDir;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getFilename()
    {
        return filename;
    }

    public void setFilename(String filename)
    {
        this.filename = filename;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public DownloadEvent getDownloadEvent()
    {
        return downloadEvent;
    }

    public void setDownloadEvent(DownloadEvent downloadEvent)
    {
        this.downloadEvent = downloadEvent;
    }

    public File getResultFile()
    {
        return resultFile;
    }

    public void setResultFile(File resultFile)
    {
        this.resultFile = resultFile;
    }
}
