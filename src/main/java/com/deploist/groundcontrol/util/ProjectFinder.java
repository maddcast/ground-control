package com.deploist.groundcontrol.util;

import com.deploist.groundcontrol.domain.Project;

import java.util.function.Predicate;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 14/08/16
 * Time: 13:24
 */
public class ProjectFinder
{
    public static Project find(Project root, Predicate<Project> predicate)
    {
        for (Project project : root.getProjects())
        {
            if (predicate.test(project))
            {
                return project;
            }
            else
            {
                Project subProject = find(project, predicate);
                if (subProject != null)
                {
                    return subProject;
                }
            }
        }

        return null;
    }
}
