package com.deploist.groundcontrol.tasks.deploy;

import com.deploist.groundcontrol.util.GzipUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Created by maddcast.
 */
public class GzipTask implements DeployTask
{
    private static final Logger log = LoggerFactory.getLogger(GzipTask.class);


    @Override
    public TaskResult work(File targetFile)
    {
        try
        {
            new GzipUtility().gzipDir(targetFile.getAbsolutePath());

            return TaskResult.ok();
        }
        catch (IOException e)
        {
            log.error("", e);

            return TaskResult.error(e);
        }
    }
}
