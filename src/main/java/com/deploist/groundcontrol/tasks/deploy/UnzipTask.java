package com.deploist.groundcontrol.tasks.deploy;

import com.deploist.groundcontrol.util.UnzipUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Created by maddcast.
 */
public class UnzipTask implements DeployTask
{
    private static final Logger log = LoggerFactory.getLogger(UnzipTask.class);


    @Override
    public TaskResult work(File targetFile)
    {
        try
        {
            new UnzipUtility().unzip(targetFile.getAbsolutePath(), targetFile.getParent());
            targetFile.delete();

            return TaskResult.ok();
        }
        catch (IOException e)
        {
            log.error("", e);

            return TaskResult.error(e);
        }
    }
}
