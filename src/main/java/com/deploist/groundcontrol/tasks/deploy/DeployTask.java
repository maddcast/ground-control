package com.deploist.groundcontrol.tasks.deploy;

import java.io.File;

/**
 * Created by maddcast.
 */
public interface DeployTask
{
    TaskResult work(File targetFile);
}
