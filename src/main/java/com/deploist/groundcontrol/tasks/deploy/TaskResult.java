package com.deploist.groundcontrol.tasks.deploy;

/**
 * Created by maddcast.
 */
public class TaskResult
{
    private boolean success;


    private TaskResult(boolean success)
    {
        this.success = success;
    }


    public static TaskResult ok()
    {
        return new TaskResult(true);
    }


    public static TaskResult error()
    {
        return new TaskResult(false);
    }


    public static TaskResult error(Exception e)
    {
        return new TaskResult(false);
    }


    public boolean isSuccess()
    {
        return success;
    }
}
