package com.deploist.groundcontrol.tasks.deploy;

import java.io.File;

/**
 * Created by maddcast.
 */
public class ChmodExecTask implements DeployTask
{
    @Override
    public TaskResult work(File targetFile)
    {
        return targetFile.setExecutable(true) ? TaskResult.ok() : TaskResult.error();
    }
}
