package com.deploist.groundcontrol.tasks;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.services.ProcessService;
import com.deploist.groundcontrol.services.VersionService;
import com.deploist.groundcontrol.util.GzipUtility;
import com.deploist.groundcontrol.util.ParamNames;
import com.deploist.groundcontrol.util.StringUtilsEx;
import com.deploist.groundcontrol.util.UnzipUtility;
import groovy.lang.Script;

import java.io.File;
import java.io.IOException;

/**
 * Created by maddcast.
 */
@SuppressWarnings("unused")
public class GroovyDeploy extends Script
{
    @Override
    public Object run()
    {
        return null;
    }


    public void unzipArtifact() throws IOException
    {
        File artifactFile = (File)getBinding().getVariable("artifactFile");

        if ((artifactFile != null) && artifactFile.isFile())
        {
            new UnzipUtility().unzip(artifactFile.getAbsolutePath(), artifactFile.getParent());
            artifactFile.delete();
        }
    }


    public void gzipDir() throws IOException
    {
        File deployDir = (File)getBinding().getVariable("deployDir");
        new GzipUtility().gzipDir(deployDir.getAbsolutePath());
    }


    public void setDefault() throws IOException
    {
        Distribution dist = (Distribution)getBinding().getVariable("dist");
        File deployDir = (File)getBinding().getVariable("deployDir");
        VersionService versionService = (VersionService)getBinding().getVariable("versionService");

        versionService.changeVersion(dist, deployDir.getName());
    }


    public boolean setExecutable(String path)
    {
        Distribution dist = (Distribution)getBinding().getVariable("dist");
        File deployDir = (File)getBinding().getVariable("deployDir");

        return new File(StringUtilsEx.joinSeamless(deployDir.getAbsolutePath() + "/", path)).setExecutable(true);
    }


    public void start() throws IOException
    {
        Distribution dist = (Distribution)getBinding().getVariable("dist");
        ProcessService processService = (ProcessService)getBinding().getVariable("processService");

        processService.run(dist, ParamNames.START_PATH);
    }


    public void stop() throws IOException
    {
        Distribution dist = (Distribution)getBinding().getVariable("dist");
        ProcessService processService = (ProcessService)getBinding().getVariable("processService");

        processService.run(dist, ParamNames.STOP_PATH);
    }


    public void restart() throws IOException
    {
        Distribution dist = (Distribution)getBinding().getVariable("dist");
        ProcessService processService = (ProcessService)getBinding().getVariable("processService");

        processService.run(dist, ParamNames.RESTART_PATH);
    }
}
