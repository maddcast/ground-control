package com.deploist.groundcontrol;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.system.ApplicationPidFileWriter;

@SpringBootApplication
public class ServerControlApplication
{
    public static void main(String[] args) throws Exception
    {
        SpringApplication app = configure(new SpringApplicationBuilder()).build();
        app.addListeners(new ApplicationPidFileWriter());
        app.run(args);
    }


    public static SpringApplicationBuilder configure(SpringApplicationBuilder application)
    {
        return application.bannerMode(Banner.Mode.OFF).sources(ServerControlApplication.class);
    }
}
