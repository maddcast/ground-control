package com.deploist.groundcontrol.models;

import com.deploist.groundcontrol.domain.Version;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 21/08/16
 * Time: 18:09
 */
public class VersionSwitchModel
{
    private String projectId;
    private List<Version> versions;
    private String currentVersion;
    private boolean restart;


    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public List<Version> getVersions()
    {
        return versions;
    }

    public void setVersions(List<Version> versions)
    {
        this.versions = versions;
    }

    public String getCurrentVersion()
    {
        return currentVersion;
    }

    public void setCurrentVersion(String currentVersion)
    {
        this.currentVersion = currentVersion;
    }

    public boolean isRestart()
    {
        return restart;
    }

    public void setRestart(boolean restart)
    {
        this.restart = restart;
    }
}
