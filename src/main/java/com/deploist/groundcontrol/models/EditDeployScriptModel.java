package com.deploist.groundcontrol.models;

/**
 * Created by maddcast.
 */
public class EditDeployScriptModel
{
    private String projectId;
    private String script;


    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getScript()
    {
        return script;
    }

    public void setScript(String script)
    {
        this.script = script;
    }
}
