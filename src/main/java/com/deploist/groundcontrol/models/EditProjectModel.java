package com.deploist.groundcontrol.models;

import com.deploist.groundcontrol.domain.Distribution;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 21:14
 */
public class EditProjectModel
{
    private String id;
    private String parentId;
    private String name;
    private Distribution dist;


    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Distribution getDist()
    {
        return dist;
    }

    public void setDist(Distribution dist)
    {
        this.dist = dist;
    }
}
