package com.deploist.groundcontrol.models;

import com.deploist.groundcontrol.domain.Version;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 21/08/16
 * Time: 18:09
 */
@Deprecated
public class VersionsListModel
{
    private String projectId;
    private List<Version> versions;
    private String currentVersion;


    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public List<Version> getVersions()
    {
        return versions;
    }

    public void setVersions(List<Version> versions)
    {
        this.versions = versions;
    }
}
