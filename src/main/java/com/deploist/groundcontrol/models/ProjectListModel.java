package com.deploist.groundcontrol.models;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.domain.Project;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 18:19
 */
public class ProjectListModel
{
    private String parentId;

    private List<Project> projects;
    private Project currentProject;
    private List<Project> projectParents;
    private Distribution dist;


    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public List<Project> getProjects()
    {
        return projects;
    }

    public void setProjects(List<Project> projects)
    {
        this.projects = projects;
    }

    public Project getCurrentProject()
    {
        return currentProject;
    }

    public void setCurrentProject(Project currentProject)
    {
        this.currentProject = currentProject;
    }

    public List<Project> getProjectParents()
    {
        return projectParents;
    }

    public void setProjectParents(List<Project> projectParents)
    {
        this.projectParents = projectParents;
    }

    public Distribution getDist()
    {
        return dist;
    }

    public void setDist(Distribution dist)
    {
        this.dist = dist;
    }
}
