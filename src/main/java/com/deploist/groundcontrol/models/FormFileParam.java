package com.deploist.groundcontrol.models;

import com.deploist.groundcontrol.util.StringUtilsEx;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 19/08/16
 * Time: 01:19
 */
public class FormFileParam
{
    private String title;
    private String rawValue;
    private String currentPath;
    private boolean exists;
    private boolean canWrite;
    private boolean canRead;
    private boolean canExecute;


    public FormFileParam(String title, String rawValue, String path)
    {
        this.title = title;
        this.rawValue = rawValue;
        this.currentPath = path;

        if (StringUtils.isNotBlank(path))
        {
            File file = new File(StringUtilsEx.parseCommandline(path).get(0));
            exists = file.exists();
            canRead = file.canRead();
            canWrite = file.canWrite();
            canExecute = file.canExecute();
        }
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getRawValue()
    {
        return rawValue;
    }

    public void setRawValue(String rawValue)
    {
        this.rawValue = rawValue;
    }

    public String getCurrentPath()
    {
        return currentPath;
    }

    public void setCurrentPath(String currentPath)
    {
        this.currentPath = currentPath;
    }

    public boolean isExists()
    {
        return exists;
    }

    public void setExists(boolean exists)
    {
        this.exists = exists;
    }

    public boolean isCanWrite()
    {
        return canWrite;
    }

    public void setCanWrite(boolean canWrite)
    {
        this.canWrite = canWrite;
    }

    public boolean isCanRead()
    {
        return canRead;
    }

    public void setCanRead(boolean canRead)
    {
        this.canRead = canRead;
    }

    public boolean isCanExecute()
    {
        return canExecute;
    }

    public void setCanExecute(boolean canExecute)
    {
        this.canExecute = canExecute;
    }
}
