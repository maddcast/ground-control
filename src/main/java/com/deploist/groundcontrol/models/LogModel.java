package com.deploist.groundcontrol.models;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 21/08/16
 * Time: 22:21
 */
public class LogModel
{
    private String projectId;
    private String log;


    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getLog()
    {
        return log;
    }

    public void setLog(String log)
    {
        this.log = log;
    }
}
