package com.deploist.groundcontrol.models;

import com.deploist.groundcontrol.domain.Version;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by maddcast.
 */
public class VersionUploadModel
{
    private String projectId;
    private List<Version> versions;

    private MultipartFile versionFile;

    private String teamcityArtifactUrl;
    private String teamcityUsername;
    private String teamcityPassword;

    private String versionNumber;
    private boolean extractArchive;
    private boolean deflateFiles;


    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public List<Version> getVersions()
    {
        return versions;
    }

    public void setVersions(List<Version> versions)
    {
        this.versions = versions;
    }

    public MultipartFile getVersionFile()
    {
        return versionFile;
    }

    public void setVersionFile(MultipartFile versionFile)
    {
        this.versionFile = versionFile;
    }

    public String getTeamcityArtifactUrl()
    {
        return teamcityArtifactUrl;
    }

    public void setTeamcityArtifactUrl(String teamcityArtifactUrl)
    {
        this.teamcityArtifactUrl = teamcityArtifactUrl;
    }

    public String getTeamcityUsername()
    {
        return teamcityUsername;
    }

    public void setTeamcityUsername(String teamcityUsername)
    {
        this.teamcityUsername = teamcityUsername;
    }

    public String getTeamcityPassword()
    {
        return teamcityPassword;
    }

    public void setTeamcityPassword(String teamcityPassword)
    {
        this.teamcityPassword = teamcityPassword;
    }
    public String getVersionNumber()
    {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber)
    {
        this.versionNumber = versionNumber;
    }

    public boolean isExtractArchive()
    {
        return extractArchive;
    }

    public void setExtractArchive(boolean extractArchive)
    {
        this.extractArchive = extractArchive;
    }

    public boolean isDeflateFiles()
    {
        return deflateFiles;
    }

    public void setDeflateFiles(boolean deflateFiles)
    {
        this.deflateFiles = deflateFiles;
    }
}
