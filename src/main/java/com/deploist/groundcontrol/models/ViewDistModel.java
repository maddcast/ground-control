package com.deploist.groundcontrol.models;

import com.deploist.groundcontrol.domain.Project;
import com.deploist.groundcontrol.domain.Version;
import com.deploist.groundcontrol.util.Starter;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 21:14
 */
public class ViewDistModel
{
    private String id;
    private String projectId;
    private Project project;
    private boolean hasIcon;
    private List<FormFileParam> fileParams;
    private String versionDirName;
    private List<Version> versions;
    private Starter starter;


    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public Project getProject()
    {
        return project;
    }

    public void setProject(Project project)
    {
        this.project = project;
    }

    public boolean isHasIcon()
    {
        return hasIcon;
    }

    public void setHasIcon(boolean hasIcon)
    {
        this.hasIcon = hasIcon;
    }

    public List<FormFileParam> getFileParams()
    {
        return fileParams;
    }

    public void setFileParams(List<FormFileParam> fileParams)
    {
        this.fileParams = fileParams;
    }

    public String getVersionDirName()
    {
        return versionDirName;
    }

    public void setVersionDirName(String versionDirName)
    {
        this.versionDirName = versionDirName;
    }

    public List<Version> getVersions()
    {
        return versions;
    }

    public void setVersions(List<Version> versions)
    {
        this.versions = versions;
    }

    public Starter getStarter()
    {
        return starter;
    }

    public void setStarter(Starter starter)
    {
        this.starter = starter;
    }
}
