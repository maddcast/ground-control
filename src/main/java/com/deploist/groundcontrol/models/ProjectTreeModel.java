package com.deploist.groundcontrol.models;

import com.deploist.groundcontrol.domain.Distribution;
import com.deploist.groundcontrol.domain.Project;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 31/08/16
 * Time: 02:14
 */
public class ProjectTreeModel
{
    private Project tree;
    private Map<String, Distribution> dists;


    public Project getTree()
    {
        return tree;
    }

    public void setTree(Project tree)
    {
        this.tree = tree;
    }

    public Map<String, Distribution> getDists()
    {
        return dists;
    }

    public void setDists(Map<String, Distribution> dists)
    {
        this.dists = dists;
    }
}
