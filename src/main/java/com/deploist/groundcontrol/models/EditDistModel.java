package com.deploist.groundcontrol.models;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created with IntelliJ IDEA.
 * User: maddcast
 * Date: 13/08/16
 * Time: 21:14
 */
public class EditDistModel
{
    private String id;
    private String projectId;
    private String basePath;
    private String deployPath;
    private String versionDirName;
    private String logPath;
    private String startPath;
    private String stopPath;
    private String restartPath;
    private String pidPath;

    private MultipartFile iconFile;


    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getProjectId()
    {
        return projectId;
    }

    public void setProjectId(String projectId)
    {
        this.projectId = projectId;
    }

    public String getBasePath()
    {
        return basePath;
    }

    public void setBasePath(String basePath)
    {
        this.basePath = basePath;
    }

    public String getDeployPath()
    {
        return deployPath;
    }

    public void setDeployPath(String deployPath)
    {
        this.deployPath = deployPath;
    }

    public String getVersionDirName()
    {
        return versionDirName;
    }

    public void setVersionDirName(String versionDirName)
    {
        this.versionDirName = versionDirName;
    }

    public String getLogPath()
    {
        return logPath;
    }

    public void setLogPath(String logPath)
    {
        this.logPath = logPath;
    }

    public String getStartPath()
    {
        return startPath;
    }

    public void setStartPath(String startPath)
    {
        this.startPath = startPath;
    }

    public String getStopPath()
    {
        return stopPath;
    }

    public void setStopPath(String stopPath)
    {
        this.stopPath = stopPath;
    }

    public String getRestartPath()
    {
        return restartPath;
    }

    public void setRestartPath(String restartPath)
    {
        this.restartPath = restartPath;
    }

    public String getPidPath()
    {
        return pidPath;
    }

    public void setPidPath(String pidPath)
    {
        this.pidPath = pidPath;
    }

    public MultipartFile getIconFile()
    {
        return iconFile;
    }

    public void setIconFile(MultipartFile iconFile)
    {
        this.iconFile = iconFile;
    }
}
