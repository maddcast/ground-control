package com.deploist.groundcontrol;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.FileInputStream;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ServerControlApplicationTests
{
    @Autowired
    private WebApplicationContext webApplicationContext;


    @Test
    public void contextLoads() throws Exception
    {
        String originalName = "Bootstrap_Metro_Dashboard-master.zip";
        String filename = "/home/maddcast/Downloads/" + originalName;
        String projectId = "206471ff-7433-4552-b973-0c01476e2294";

        MockMultipartFile multipartFile = new MockMultipartFile("versionFile", filename, null, new FileInputStream(filename));

        MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        mockMvc.perform(MockMvcRequestBuilders
                .fileUpload("/versions/upload/zip")
                .file(multipartFile)
                .param("projectId", projectId)
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }
}
